module gitlab.com/colinwilson/colinwilson.gitlab.io

go 1.20

require (
	github.com/colinwilson/lotusdocs v0.0.0-20230824022610-a7d660221d18 // indirect
	github.com/gohugoio/hugo-mod-bootstrap-scss/v5 v5.20300.20003 // indirect
)
